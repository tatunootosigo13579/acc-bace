# Acc-Base

## Acc-Baseの場所
```
/var/www/html/acc-base
```

## このアプリをサーバーで実行する為にサーバーにインストールしなければならないもの
- node.js (https://nodejs.org/ja/)
- pm2 (https://pm2.keymetrics.io/)
```
npm install pm2 -g
```

## このアプリをPCで実行する為に必要なもの
- node.js (https://nodejs.org/ja/)

## 環境構築
```
npm install
```

## サーバー実行コード
```
cd
cd /var/www/html/acc-base
// 初回起動
pm2 start npm --name acc-base -- start

// 2回目以降
pm2 start 0
```

## ファイル構成

- `bin`
  - Node.jsの設定ファイルが入っています。
- `public`
  - Web公開されるファイルが入っています。
- `routes`
  - このアプリケーションのサーバー側のファイルが入っています。(Web公開されません。)
    - `acc_data`
      - ここに加速度計のデータがあります。
- `views`
  - このアプリケーションのフロントエンド側のファイルが入っています。
- `.gitignore`
  - Gitのコミット対象外リストです。
- `app.js`
  - サーバー側の設定ファイルです。
- `package.json`
  - 依存するnpmパッケージに関する設定ファイルです。
- `README.md`
  - このファイルです。

## 実行
- `npm start`
  - アプリが立ち上がります。

## 使用言語・フレームワークなど
- HTML
- CSS
- JavaScript
- WebGL
- Google Maps
- Node.js
- Express

## 対応ブラウザ
- 各種ブラウザ最新バージョン

