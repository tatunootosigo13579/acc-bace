/*
    バイナリファイル1Byte目の時間計算
*/

let year, month, day, hour, minute, second;

function calculateDate ( totalSecond ) {

    const day_Second = calculateYearMonthDay( totalSecond );
    hour = Math.floor(day_Second / 3600);
    const hour_Second = day_Second % 3600;
    minute = Math.floor(hour_Second / 60);
    second = hour_Second % 60;

    if ( month < 10 ) {
        month = '0' + month;
    }
    if ( day < 10 ) {
        day = '0' + day;
    }
    if ( hour < 10 ) {
        hour = '0' + hour;
    }
    if ( minute < 10 ) {
        minute = '0' + minute;
    }
    if ( second < 10 ) {
        second = '0' + second;
    }

    return { y: year, mo: month, d: day, h: hour, mi: minute, s: second };
}

function calculateYearMonthDay( totalSecond ) {
    let days;
    let  y, m, d, lpy;
    days = totalSecond / 86400;

    for ( y = 2016; days >= 0; y++ ) {
        days -= 365 + leapYear( y );
    }

    y--;
    lpy = leapYear( y );
    days += 365 + lpy;
    year = y;

    if ( days < 60 ) {

        if ( days < 31 ) {
            m = 1;
            d = days;
        }
        else {
            m = 2;
            d = days - 31;
            if ( lpy == 0 && d == 28 ) {
                m = 3;
                d = 0;
            }
        }

    }
    else {
        days -= 59 + lpy;

        if ( days < 31 ) {
            m = 3;
            d = days;
        }
        else if ( days < 61 ) {
            m = 4;
            d = days - 31;
        }
        else if ( days < 92 ) {
            m = 5;
            d = days - 61;
        }
        else if ( days < 122 ) {
            m = 6;
            d = days - 92;
        }
        else if ( days < 153 ) {
            m = 7;
            d = days - 122;
        }
        else if (days < 184) {
            m = 8;
            d = days - 153;
        }
        else if ( days < 214 ) {
            m = 9;
            d = days - 184;
        }
        else if ( days < 245 ) {
            m = 10;
            d = days - 214;
        }
        else if ( days < 275 ) {
            m = 11;
            d = days - 245;
        }
        else {
            m = 12;
            d = days - 275;
        }

    }

    month = m;
    day = Math.floor( d + 1 );

    return totalSecond % 86400;
}

function leapYear( year ) {

    if ( year % 4 != 0 ) {
        return 0;
    }
    else {
        return 1;
    }
}