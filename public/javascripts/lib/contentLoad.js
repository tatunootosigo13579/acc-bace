// グラフの罫線
const boxLines = [
    // 横線
    -1, 0.5, 0.0,
    1, 0.5, 0.0,

    -1, 0.0, 0.0,
    1, 0.0, 0.0,

    -1, -0.5, 0.0,
    1, -0.5, 0.0,


    // 縦線
    -0.5, 1, 0.0,
    -0.5, -1, 0.0,

    0.0, 1, 0.0,
    0.0, -1, 0.0,

    0.5, 1, 0.0,
    0.5, -1, 0.0
];

// グラフの罫線の色
const boxColors = [
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0,
    1.0, 1.0, 0.0, 1.0
];

// canvasとwebglの呼び出し
const dx_c = document.getElementById("dx_canvas");
const dx_gl = dx_c.getContext('webgl') || dx_c.getContext('experimental-webgl');
if ( !dx_gl ) alert( "It failed making gl." );

const dy_c = document.getElementById("dy_canvas");
const dy_gl = dy_c.getContext('webgl') || dy_c.getContext('experimental-webgl');
if( !dy_gl ) alert( "It failed making gl." );

const dz_c = document.getElementById("dz_canvas");
const dz_gl = dz_c.getContext('webgl') || dz_c.getContext('experimental-webgl');
if ( !dz_gl ) alert( "It failed making gl." );

// 頂点シェーダーとフラグメントシェーダーの用意
const dx_vs = create_shader( dx_gl, 'vs' );
const dx_fs = create_shader( dx_gl, 'fs' );
const dx_prg = create_program( dx_gl, dx_vs, dx_fs );
let dx_attL = [];
dx_attL[0] = dx_gl.getAttribLocation( dx_prg, 'position' );
dx_attL[1] = dx_gl.getAttribLocation( dx_prg, 'color' );
const dx_attS = [ 3, 4 ];
const dy_vs = create_shader( dy_gl, 'vs' );
const dy_fs = create_shader( dy_gl, 'fs' );
const dy_prg = create_program( dy_gl, dy_vs, dy_fs );
let dy_attL = [];
dy_attL[0] = dy_gl.getAttribLocation( dy_prg, 'position' );
dy_attL[1] = dy_gl.getAttribLocation( dy_prg, 'color' );
const dy_attS = [ 3, 4 ];
const dz_vs = create_shader( dz_gl, 'vs' );
const dz_fs = create_shader( dz_gl, 'fs' );
const dz_prg = create_program( dz_gl, dz_vs, dz_fs );
let dz_attL = [];
dz_attL[0] = dz_gl.getAttribLocation( dz_prg, 'position' );
dz_attL[1] = dz_gl.getAttribLocation( dz_prg, 'color' );
const dz_attS = [ 3, 4 ];

// 描画の際に使用
let x_dataPosition, y_dataPosition, z_dataPosition;
let x_dataColor, y_dataColor, z_dataColor;
let x_dataVBOList, y_dataVBOList, z_dataVBOList;
let x_boxPosition, y_boxPosition, z_boxPosition;
let x_boxColor, y_boxColor, z_boxColor;
let x_boxVBOList, y_boxVBOList, z_boxVBOList;

// XYZブロック毎にとったデータをまとめる
let x_positionData, y_positionData, z_positionData;

// ↑のデータの数を記録するために使用
let p_count;

// WebGLで実際に描画する値
let timeData, x_graphData, y_graphData, z_graphData, graphColor;

// 平均を求める際に使用
let x_allTotal, y_allTotal, z_allTotal, x_average, y_average, z_average;

// 最大値最小値を求める際に使用
let x_max, x_min, y_max, y_min, z_max, z_min;

// 経過時間を入れる
let date;

// ファイルの開始・終了時間を入れる
let beginTime, endTime;

// ラジオボタン要素呼び出し
const radio = document.getElementsByName("radioButton");

// ラジオボタン切り替えで使用する
let beforeButton;

function contentLoad( content, nextDataStart ) {

    x_positionData = []; y_positionData = []; z_positionData = [];
    p_count = 0;
    timeData = []; x_graphData = []; y_graphData = []; z_graphData = []; graphColor = [];
    x_allTotal = 0; y_allTotal = 0; z_allTotal = 0; x_average = 0; y_average = 0; z_average = 0;
    x_max = 0; x_min = 0; y_max = 0; y_min = 0; z_max = 0; z_min = 0;


    // 指定されたGal（縦）でグラフを描画
    const galHeight = Number( document.getElementById("gal_value").value );

    //読み込むファイルの数
    const listHour = Number( document.getElementById("listHour").value );

    // 8bit毎に分ける
    const tmpArray = new Uint8Array( content );

    // バイナリデータの日付データの位置
    let nowByte = 0;

    // ラジオボタンを1hに戻す
    for ( const element of radio ) {
        element.checked = false;
    }
    radio[0].checked = true;

    //データ読み込みを終了するタイミング
    readFinishCount = listHour;
    nowReadFinishCount = 1;

    //データが日付をまたぐ場合のフラグ処理
    let is_beyondDay = false;

    // ファイル解析
    while ( 1 ) {

        // データがFF FF(255 255)となっていたらバイナリ読み込みを終了する
        // データを2時間分読み込む場合はreadFinishCountとnowReadFinishCountの値で続行終了を分ける
        if ( tmpArray[ nowByte ] === 255 && tmpArray[ nowByte + 1 ] === 255 && readFinishCount === nowReadFinishCount ) {
            document.getElementById('time_finish').value = date.h + ":" + date.mi + ":" + date.s;
            is_beyondDay = false;
            break;
        }
        else if( tmpArray[ nowByte ] === 255 && tmpArray[ nowByte + 1 ] === 255 && readFinishCount != nowReadFinishCount ) {
            nowReadFinishCount++;
            nowByte = nextDataStart;
            // 日付を跨いで表示する場合はis_beyondDayをtrueにする
            // 例：7/1 23:00 ~ 7/2 0:59
            if ( date.h === 23 ) {
                is_beyondDay = true;
            }
        }

        // バイナリの時間を処理するために4byte分用意する
        const timeFourBuffer = new ArrayBuffer(4);
        let time = new DataView( timeFourBuffer );
        // time([3] [2] [1] [0])
        // バイナリファイルに(FA CD 23 3E)が入っているならば
        // time(FA CD 23 3E)となる
        time.setUint8( 0, tmpArray[ nowByte + 0 ] );
        time.setUint8( 1, tmpArray[ nowByte + 1 ] );
        time.setUint8( 2, tmpArray[ nowByte + 2 ] );
        time.setUint8( 3, tmpArray[ nowByte + 3 ] );

        // 経過秒数の算出
        date = calculateDate( time.getUint32( 0, true ) );
        // データを読み込み始めたばかりの場合はデータの最初の時間を記録しておく
        if ( nowByte == 0 ) {
            document.getElementById('time_start').value = date.h + ":" + date.mi + ":" + date.s;
        }

        // 秒と分と時を合わせた値を保持しておく
        let second;
        // 日付を超える場合は1日分時間を足す
        if ( is_beyondDay === true ) {
            // グラフの時間は日付まで設定する欄が存在しないので
            // 23:00 ~ 0:59だとエラーになる
            // そのため、内部は23:00 ~ 24:59で計算する
            second = Number( date.h ) * 3600 + Number( date.mi ) * 60 + Number( date.s ) + 24 * 3600;
        }
        else {
            second = Number( date.h ) * 3600 + Number( date.mi ) * 60 + Number( date.s );
        }

        // メモリ解放
        time = null;





/*
    条件分け
*/


        // 5Byte目をフォーマットに則り区切る
        const conditions = tmpArray[ nowByte + 5 ].toString(2);
        const compression_condition = conditions.slice( 0,2 ); // 圧縮条件
        const gain_condition = conditions.slice( 2,4 );        // Gain
        const sampling_condition = conditions.slice( 5,8 );    // サンプル数
        let is_compression, gain, is_sampling;

        // 圧縮条件
        switch ( compression_condition ) {

            // 0(00): 16bitsAbsolute
            case '00':
                is_compression = 0;
                break;

            // 1(01): 16bits-8bits(def.)
            case '01':
                is_compression = 1;
                break;

            // 2(10): 24bits-16bits(def.)
            case '10':
                is_compression = 2;
                break;

            // 3(11): 24bits-8bits(def.)
            case '11':
                is_compression = 3;
                break;

            default:
                break;

        }

        // Gain
        switch ( gain_condition ) {

            // 0(00): 0.1gal/LSB
            case '00':
                gain = 0.1;
                break;

            // 1(01): 0.01 gal/LSB
            case '01':
                gain = 0.01;
                break;

            // 2(10): 0.005 gal/LSB
            case '10':
                gain = 0.005;
                break;

            // 3(11): 0.001 gal/LSB
            case '11':
                gain = 0.001;
                break;

            default:
                break;

        }

        // サンプル数
        switch ( sampling_condition ) {

            // 0:0sample
            case '000':
                is_sampling = 0;
                break;

            // 1::Average
            case '001':
                is_sampling = 1;
                break;

            // 2:Minimum+Maximum
            case '010':
                is_sampling = 2;
                break;

            // 3: 20 samples/sec
            case '011':
                is_sampling = 3;
                break;

            // 4: 50 samples/sec
            case '100':
                is_sampling = 4;
                break;

            // 5: 100 samples/sec
            case '101':
                is_sampling = 5;
                break;

            // 6: 200 samples/sec
            case '110':
                is_sampling = 6;
                break;

            default:
                break;

        }




        // バイナリデータの処理に使用
        // 初期値
        let x_graphDataFourBuffer, y_graphDataFourBuffer, z_graphDataFourBuffer;
        let x_zeroBuffer, y_zeroBuffer, z_zeroBuffer;
        // 差分
        let x_graphDataOneBuffer, y_graphDataOneBuffer, z_graphDataOneBuffer;
        let x_add, y_add, z_add;

        // バイナリから直した値を入れる配列
        let x_position = [], y_position = [], z_position = [];

        // ブロック毎の合計値
        let x_blockTotal = 0, y_blockTotal = 0, z_blockTotal = 0;



/*
    条件ごとに加速度データを処理する
*/

        switch ( is_compression ) {
            // 0(00): 16bitsAbsolute,  1(01): 16bits-8bits(def.)は未実装
            case 0:
            case 1:
                break;

            // 2(10): 24bits-16bits(def.), 3(11): 24bits-8bits(def.)の場合
            case 2:
            case 3:
                // x_zeroBuffer([3] [2] [1] [0])
                // 初期値は3Byteで保存されているが
                // 3Byteのバイナリを処理する機能は存在しないため
                // 4Byteの領域を確保し、[2]の2桁目から条件に沿って先頭にFFか00を付け足す

                // バイナリファイル1(AB CD FE), バイナリファイル2(12 34 75)
                // x_zeroBuffer1(FF FE CD AB), x_zeroBuffer2(00 75 34 12)
                x_graphDataFourBuffer = new ArrayBuffer(4);
                y_graphDataFourBuffer = new ArrayBuffer(4);
                z_graphDataFourBuffer = new ArrayBuffer(4);
                x_zeroBuffer = new DataView( x_graphDataFourBuffer );
                y_zeroBuffer = new DataView( y_graphDataFourBuffer );
                z_zeroBuffer = new DataView( z_graphDataFourBuffer );





                // X初期値の処理
                x_zeroBuffer.setInt8( 0, tmpArray[ nowByte + 6 ] );
                x_zeroBuffer.setInt8( 1, tmpArray[ nowByte + 7 ] );
                x_zeroBuffer.setInt8( 2, tmpArray[ nowByte + 8 ] );

                // 8Byte目の2桁目のみを抽出する
                // 08 -> 0, FA -> F
                const x_zeroBufferTop = ( x_zeroBuffer.getInt8( 2, true ) >>> 0 ).toString(16).toUpperCase().slice( 0, 1 );
                // 抽出した数字・文字が8以上ならFFを入れる
                if ( Number( parseInt( x_zeroBufferTop, 16 ) ) >= 8 ) {
                    x_zeroBuffer.setInt8( 3, 255 );
                }
                // 8未満なら0を入れる
                else {
                    x_zeroBuffer.setInt8( 3, 0 );
                }

                // Xの初期値を入れる
                x_position[0] = Number( x_zeroBuffer.getInt32( 0, true ) );


                // Y初期値の処理
                y_zeroBuffer.setInt8( 0, tmpArray[ nowByte + 9 ] );
                y_zeroBuffer.setInt8( 1, tmpArray[ nowByte + 10 ] );
                y_zeroBuffer.setInt8( 2, tmpArray[ nowByte + 11 ] );

                const y_zeroBufferTop = ( y_zeroBuffer.getInt8( 2, true ) >>> 0 ).toString(16).toUpperCase().slice( 0, 1 );
                if ( Number( parseInt( y_zeroBufferTop, 16 ) ) >= 8 ) {
                    y_zeroBuffer.setInt8( 3, 255 );
                } else {
                    y_zeroBuffer.setInt8( 3, 0 );
                }

                // Yの初期値を入れる
                y_position[0] = Number( y_zeroBuffer.getInt32( 0, true ) );


                // Z初期値の処理
                z_zeroBuffer.setInt8( 0, tmpArray[ nowByte + 12 ] );
                z_zeroBuffer.setInt8( 1, tmpArray[ nowByte + 13 ] );
                z_zeroBuffer.setInt8( 2, tmpArray[ nowByte + 14 ] );

                const z_zeroBufferTop = ( z_zeroBuffer.getInt8( 2, true ) >>> 0 ).toString(16).toUpperCase().slice( 0, 1 );
                if ( Number( parseInt( z_zeroBufferTop, 16 ) ) >= 8 ) {
                    z_zeroBuffer.setInt8( 3, 255 );
                } else {
                    z_zeroBuffer.setInt8( 3, 0 );
                }

                // Zの初期値を入れる
                z_position[0] = Number( z_zeroBuffer.getInt32( 0, true ) );





                // 平均値を求める際に使用
                x_blockTotal += x_position[0];
                y_blockTotal += y_position[0];
                z_blockTotal += z_position[0];

                // canvasに描画する方
                timeData[p_count] = second;
                x_positionData[p_count] = x_position[0] * gain;
                y_positionData[p_count] = y_position[0] * gain;
                z_positionData[p_count] = z_position[0] * gain;

                // データ解析を始めたばかりの場合は後に計算する最大値と最小値にとりあえず入れておく
                if ( p_count === 0 ) {
                    x_max = x_positionData[p_count];
                    x_min = x_positionData[p_count];
                    y_max = y_positionData[p_count];
                    y_min = y_positionData[p_count];
                    z_max = z_positionData[p_count];
                    z_min = z_positionData[p_count];
                }

                // 配列の場所を次に移動
                p_count++;

                break;

            default:
                break;
        }

        // メモリ解放
        x_zeroBuffer = null;
        y_zeroBuffer = null;
        z_zeroBuffer = null;




        // 0(00): 16bitsAbsolute未実装
        if ( is_compression === 0 ) {
            nowByte += 606;
        }

        // 1(01): 16bits-8bits(def.)未実装
        else if ( is_compression === 1 ) {
            nowByte += 310;
        }

        // 2(10): 24bits-16bits(def.)
        else if ( is_compression === 2 ) {
            // バイナリのXYZ増減値を処理するためにそれぞれ2byte用意する
            x_graphDataOneBuffer = new ArrayBuffer(2);
            y_graphDataOneBuffer = new ArrayBuffer(2);
            z_graphDataOneBuffer = new ArrayBuffer(2);
            x_add = new DataView( x_graphDataOneBuffer );
            y_add = new DataView( y_graphDataOneBuffer );
            z_add = new DataView( z_graphDataOneBuffer );

            switch ( is_sampling ) {
                // 2:Minimum+Maximum
                case 2:
                    // maxの値を求める
                    // x_add([1][0])
                    // バイナリファイルに(AB 12)が入っているならば
                    // x_add(AB 12)となる
                    x_add.setUint8( 1, tmpArray[ nowByte + 16 ] );
                    x_add.setUint8( 0, tmpArray[ nowByte + 17 ] );
                    y_add.setUint8( 1, tmpArray[ nowByte + 18 ] );
                    y_add.setUint8( 0, tmpArray[ nowByte + 19 ] );
                    z_add.setUint8( 1, tmpArray[ nowByte + 20 ] );
                    z_add.setUint8( 0, tmpArray[ nowByte + 21 ] );
                    x_position[1] = Number( x_add.getUint16(0) );
                    y_position[1] = Number( y_add.getUint16(0) );
                    z_position[1] = Number( z_add.getUint16(0) );
                    x_blockTotal += x_position[0] + x_position[1];
                    y_blockTotal += y_position[0] + y_position[1];
                    z_blockTotal += z_position[0] + z_position[1];

                    // canvasに描画する方
                    timeData[p_count] = second + 0.5;
                    x_positionData[p_count] = ( x_position[0] + x_position[1] ) * gain;
                    y_positionData[p_count] = ( y_position[0] + y_position[1] ) * gain;
                    z_positionData[p_count] = ( z_position[0] + z_position[1] ) * gain;
                    p_count++;

                    nowByte += 22;
                    break;

                // 5: 100 samples/sec
                case 5:
                    // x_add([1][0])
                    // バイナリファイルに(CD 34)が入っているならば
                    // x_add(CD 34)となる
                    for ( let i = 16, j = 1; j <= 99; i+=6, j++ ) {
                        x_add.setInt8( 1, tmpArray[ nowByte + i ] );
                        x_add.setInt8( 0, tmpArray[ nowByte + i + 1 ] );
                        y_add.setInt8( 1, tmpArray[ nowByte + i + 2 ] );
                        y_add.setInt8( 0, tmpArray[ nowByte + i + 3 ] );
                        z_add.setInt8( 1, tmpArray[ nowByte + i + 4 ] );
                        z_add.setInt8( 0, tmpArray[ nowByte + i + 5 ] );
                        x_position[ j ] = x_position[ j - 1 ] + Number( x_add.getInt16(0) );
                        y_position[ j ] = y_position[ j - 1 ] + Number( y_add.getInt16(0) );
                        z_position[ j ] = z_position[ j - 1 ] + Number( z_add.getInt16(0) );
                        x_blockTotal += x_position[ j ];
                        y_blockTotal += y_position[ j ];
                        z_blockTotal += z_position[ j ];

                        // canvasに描画する方
                        timeData[p_count] = second + 0.01 * j;
                        x_positionData[p_count] = x_position[j] * gain;
                        y_positionData[p_count] = y_position[j] * gain;
                        z_positionData[p_count] = z_position[j] * gain;
                        p_count++;
                    }
                    nowByte += 610;
                    break;

                default:
                    break;
            }
        }

        // 3(11): 24bits-8bits(def.)
        else if ( is_compression === 3 ) {
            // バイナリのXYZ増減値を処理するためにそれぞれ2byte用意する
            x_graphDataOneBuffer = new ArrayBuffer(1);
            y_graphDataOneBuffer = new ArrayBuffer(1);
            z_graphDataOneBuffer = new ArrayBuffer(1);
            x_add = new DataView( x_graphDataOneBuffer );
            y_add = new DataView( y_graphDataOneBuffer );
            z_add = new DataView( z_graphDataOneBuffer );

            switch ( is_sampling ) {
                // 2:Minimum+Maximum
                case 2:
                    // maxの値を求める
                    // x_add([0])
                    // バイナリファイルに(A1)が入っているならば
                    // x_add(A1)となる
                    x_add.setUint8( 0, tmpArray[ nowByte + 15 ] );
                    y_add.setUint8( 0, tmpArray[ nowByte + 16 ] );
                    z_add.setUint8( 0, tmpArray[ nowByte + 17 ] );
                    x_position[1] = Number( x_add.getUint8(0) );
                    y_position[1] = Number( y_add.getUint8(0) );
                    z_position[1] = Number( z_add.getUint8(0) );
                    x_blockTotal += x_position[0] + x_position[1];
                    y_blockTotal += y_position[0] + y_position[1];
                    z_blockTotal += z_position[0] + z_position[1];

                    // canvasに描画する方
                    timeData[p_count] = second + 0.5;
                    x_positionData[p_count] = ( x_position[0] + x_position[1] ) * gain;
                    y_positionData[p_count] = ( y_position[0] + y_position[1] ) * gain;
                    z_positionData[p_count] = ( z_position[0] + z_position[1] ) * gain;
                    p_count++;

                    nowByte += 18;
                    break;


                // 5: 100 samples/sec
                case 5:
                    // x_add([0])
                    // バイナリファイルに(B2)が入っているならば
                    // x_add(B2)となる
                    for ( let i = 15, j = 1; j <= 99; i+=3, j++ ) {
                        x_add.setInt8( 0, tmpArray[ nowByte + i ] );
                        y_add.setInt8( 0, tmpArray[ nowByte + i + 1 ] );
                        z_add.setInt8( 0, tmpArray[ nowByte + i + 2 ] );
                        x_position[j] = x_position[ j - 1 ] + Number( x_add.getInt8(0) );
                        y_position[j] = y_position[ j - 1 ] + Number( y_add.getInt8(0) );
                        z_position[j] = z_position[ j - 1 ] + Number( z_add.getInt8(0) );
                        x_blockTotal += x_position[j];
                        y_blockTotal += y_position[j];
                        z_blockTotal += z_position[j];

                        // canvasに描画する方
                        timeData[p_count] = second + 0.01 * j;
                        x_positionData[p_count] = x_position[j] * gain;
                        y_positionData[p_count] = y_position[j] * gain;
                        z_positionData[p_count] = z_position[j] * gain;
                        p_count++;
                    }
                    nowByte += 312;
                    break;

                default:
                    break;
            }
        }

        x_allTotal += x_blockTotal * gain;
        y_allTotal += y_blockTotal * gain;
        z_allTotal += z_blockTotal * gain;

        // メモリ解放
        x_add = null;
        y_add = null;
        z_add = null;

    }





    // XYZの平均値を求める
    x_average = ( x_allTotal / x_positionData.length ).toFixed(1);
    y_average = ( y_allTotal / y_positionData.length ).toFixed(1);
    z_average = ( z_allTotal / z_positionData.length ).toFixed(1);
    document.getElementById("dx_average").value = x_average;
    document.getElementById("dy_average").value = y_average;
    document.getElementById("dz_average").value = z_average;

    // XYZのグラフ上下の値
    document.getElementById("dx_top").innerHTML = ( Number(x_average) + galHeight ).toFixed(1);
    document.getElementById("dy_top").innerHTML = ( Number(y_average) + galHeight ).toFixed(1);
    document.getElementById("dz_top").innerHTML = ( Number(z_average) + galHeight ).toFixed(1);

    document.getElementById("dx_bottom").innerHTML = ( Number(x_average) - galHeight ).toFixed(1);
    document.getElementById("dy_bottom").innerHTML = ( Number(y_average) - galHeight ).toFixed(1);
    document.getElementById("dz_bottom").innerHTML = ( Number(z_average) - galHeight ).toFixed(1);

    // 最大値と最小値の取得
    for ( let i = 0; i < x_positionData.length; i++ ) {
        // X
        if ( x_max < x_positionData[i] ) {
            x_max = x_positionData[i];
        }
        if ( x_min > x_positionData[i] ) {
            x_min = x_positionData[i];
        }

        // Y
        if ( y_max < y_positionData[i] ) {
            y_max = y_positionData[i];
        }
        if ( y_min > y_positionData[i] ) {
            y_min = y_positionData[i];
        }

        // Z
        if ( z_max < z_positionData[i] ) {
            z_max = z_positionData[i];
        }
        if ( z_min > z_positionData[i] ) {
            z_min = z_positionData[i];
        }
    }

    // データmaxの値
    document.getElementById("dx_max").value = (x_max).toFixed(1);
    document.getElementById("dy_max").value = (y_max).toFixed(1);
    document.getElementById("dz_max").value = (z_max).toFixed(1);

    // データminの値
    document.getElementById("dx_min").value = (x_min).toFixed(1);
    document.getElementById("dy_min").value = (y_min).toFixed(1);
    document.getElementById("dz_min").value = (z_min).toFixed(1);


    /*
        以下しばらくWebGL関連
    */


    // canvasの横幅（時間）調整
    let startHMS = document.getElementById('time_start').value.split(':');
    let startHH = Number( startHMS[0] );
    let startMM = Number( startHMS[1] );
    if ( startHMS[2] === undefined ) {
        startHMS[2] = '00';
    }
    let startSS = Number( startHMS[2] );
    // 計測を始めた時間(秒)
    let startTime = startHH * 3600 + startMM * 60 + startSS;
    beginTime = startTime;
    let finishHMS = document.getElementById('time_finish').value.split(':');
    let finishHH;
    if ( startHH === 23 && listHour === 2 ) {
        finishHH = Number( finishHMS[0] ) + 24;
    }
    else {
        finishHH = Number( finishHMS[0] );
    }
    let finishMM = Number( finishHMS[1] );
    if ( finishHMS[2] === undefined ) {
        finishHMS[2] = '00';
    }
    let finishSS = Number( finishHMS[2] );
    // 計測を終えた時間(秒)
    let finishTime = finishHH * 3600 + finishMM * 60 + finishSS;
    endTime = finishTime;
    // XYZそれぞれの計測した時間をWebGLの横幅に合わせる
    let x_timeRatio = ( finishTime - startTime ) / 2;
    let y_timeRatio = ( finishTime - startTime ) / 2;
    let z_timeRatio = ( finishTime - startTime ) / 2;

    // canvas用の座標を設定する
    // WebGLは縦と横どちらも-1~1の範囲であるため、最大値と最小値が1, -1になるようにする
    for ( let i = 0; i < timeData.length; i++ ) {
        x_graphData.push( ( timeData[i] - startTime ) / x_timeRatio - 1, ( x_positionData[i] - x_average ) / galHeight, 0 );
        y_graphData.push( ( timeData[i] - startTime ) / y_timeRatio - 1, ( y_positionData[i] - y_average ) / galHeight, 0 );
        z_graphData.push( ( timeData[i] - startTime ) / z_timeRatio - 1, ( z_positionData[i] - z_average ) / galHeight, 0 );
        graphColor.push( 0.0, 1.0, 1.0, 1.0 );
    }





    // 頂点・色情報設定
    x_dataPosition = create_vbo( dx_gl, x_graphData );
    y_dataPosition = create_vbo( dy_gl, y_graphData );
    z_dataPosition = create_vbo( dz_gl, z_graphData );
    x_dataColor = create_vbo( dx_gl, graphColor );
    y_dataColor = create_vbo( dy_gl, graphColor );
    z_dataColor = create_vbo( dz_gl, graphColor );
    x_dataVBOList = [ x_dataPosition, x_dataColor ];
    y_dataVBOList = [ y_dataPosition, y_dataColor ];
    z_dataVBOList = [ z_dataPosition, z_dataColor ];

    // 目安の設定
    x_boxPosition = create_vbo( dx_gl, boxLines );
    y_boxPosition = create_vbo( dy_gl, boxLines );
    z_boxPosition = create_vbo( dz_gl, boxLines );
    x_boxColor = create_vbo( dx_gl, boxColors );
    y_boxColor = create_vbo( dy_gl, boxColors );
    z_boxColor = create_vbo( dz_gl, boxColors );
    x_boxVBOList = [ x_boxPosition, x_boxColor ];
    y_boxVBOList = [ y_boxPosition, y_boxColor ];
    z_boxVBOList = [ z_boxPosition, z_boxColor ];

    //canvas初期化
    dx_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    dx_gl.clear( dx_gl.COLOR_BUFFER_BIT );
    dy_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    dy_gl.clear( dy_gl.COLOR_BUFFER_BIT );
    dz_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    dz_gl.clear( dz_gl.COLOR_BUFFER_BIT );

    // 描画
    dx_gl.useProgram( dx_prg );
    set_attribute( dx_gl, x_boxVBOList, dx_attL, dx_attS );
    dx_gl.drawArrays( dx_gl.LINES, 0, boxLines.length / 3 );
    set_attribute( dx_gl, x_dataVBOList, dx_attL, dx_attS );
    dx_gl.drawArrays( dx_gl.LINE_STRIP, 0, x_graphData.length / 3 );

    dy_gl.useProgram( dy_prg );
    set_attribute( dy_gl, y_boxVBOList, dy_attL, dy_attS );
    dy_gl.drawArrays( dy_gl.LINES, 0, boxLines.length / 3 );
    set_attribute( dy_gl, y_dataVBOList, dy_attL, dy_attS );
    dy_gl.drawArrays( dy_gl.LINE_STRIP, 0, y_graphData.length / 3 );

    dz_gl.useProgram( dz_prg );
    set_attribute( dz_gl, z_boxVBOList, dz_attL, dz_attS );
    dz_gl.drawArrays( dz_gl.LINES, 0, boxLines.length / 3 );
    set_attribute( dz_gl, z_dataVBOList, dz_attL, dz_attS );
    dz_gl.drawArrays( dz_gl.LINE_STRIP, 0, z_graphData.length / 3 );

    /*
        ここまでWebGL
    */

    // メモリ解放の為、中身を空に
    x_graphData = [];
    y_graphData = [];
    z_graphData = [];

    // 時間調整のラジオボタンを出現させる
    document.getElementById("time_radioButton").style.opacity = "1";

    // ダウンロードボタンの上に文字を表示する
    document.getElementById("downloadText_top").innerHTML = document.getElementById("acc_place").value + ' ' + document.getElementById("acc_date").value + '-' + downloadDay;
    document.getElementById("downloadText_bottom").innerHTML = document.getElementById("time_start").value + ' ~ ' + document.getElementById("time_finish").value;

    // ダウンロードボタンを表示する
    document.getElementById("download").style.opacity = "1";

    // ラジオボタンの1hを選択した扱いにする
    beforeButton = radio[0];

    document.getElementById("button0m").disabled = false;
    document.getElementById("button15m").disabled = false;
    document.getElementById("button30m").disabled = false;
    document.getElementById("button45m").disabled = false;



/*
    ファイルのデータが1時間分ない場合のラジオボタン処理
*/

    // データの開始時間が15分より後
    if ( startHMS[1] >= 15 ) {
        document.getElementById("button0m").disabled = true;
    }
    // データの開始時間が30分より後
    else if ( startHMS[1] >= 30 ) {
        document.getElementById("button0m").disabled = true;
        document.getElementById("button15m").disabled = true;
    }
    // データの開始時間が45分より後
    else if ( startHMS[1] >= 45 ) {
        document.getElementById("button0m").disabled = true;
        document.getElementById("button15m").disabled = true;
        document.getElementById("button30m").disabled = true;
    }

    // データの終了時間が45分未満
    if ( finishHMS[1] < 45 ) {
        document.getElementById("button45m").disabled = true;
    }
    // データの終了時間が30分未満
    else if ( finishHMS[1] < 30 ) {
        document.getElementById("button45m").disabled = true;
        document.getElementById("button30m").disabled = true;
    }
    // データの終了時間が15分未満
    else if ( finishHMS[1] < 15 ) {
        document.getElementById("button45m").disabled = true;
        document.getElementById("button30m").disabled = true;
        document.getElementById("button15m").disabled = true;
    }

}

/*
    Gal幅や時間が変更された際の処理
*/

function changeValue() {

    let x_timeRatio, y_timeRatio, z_timeRatio, dataStart, dataFinish, startTime, finishTime, beginHour, beginMinute, beginSecond, endHour, endMinute, endSecond;

    // Gal
    const galHeight = Number( document.getElementById("gal_value").value );

    // XYZのグラフ上下の値
    document.getElementById("dx_top").innerHTML = ( Number( x_average ) + galHeight ).toFixed(1);
    document.getElementById("dy_top").innerHTML = ( Number( y_average ) + galHeight ).toFixed(1);
    document.getElementById("dz_top").innerHTML = ( Number( z_average ) + galHeight ).toFixed(1);

    document.getElementById("dx_bottom").innerHTML = ( Number( x_average ) - galHeight ).toFixed(1);
    document.getElementById("dy_bottom").innerHTML = ( Number( y_average ) - galHeight ).toFixed(1);
    document.getElementById("dz_bottom").innerHTML = ( Number( z_average ) - galHeight ).toFixed(1);

    beginHour = Math.floor( beginTime / 3600 );
    if ( beginHour < 10 ) {
        beginHour = "0" + beginHour;
    }
    const beginMinuteSecond = beginTime % 3600;
    beginMinute = Math.floor( beginMinuteSecond / 60 );
    beginSecond = beginMinuteSecond % 60;

    endHour = Math.floor( endTime / 3600 );
    if ( endHour < 10 ) {
        endHour = "0" + endHour;
    }
    const endMinuteSecond = endTime % 3600;
    endMinute = Math.floor( endMinuteSecond / 60 );
    endSecond = endMinuteSecond % 60;

    const readFileLength = Number( document.getElementById("listHour").value );

    const endValue = endMinuteSecond + 3600 * ( readFileLength - 1 );

    // ラジオボタンが1hの時
    if ( radio[0].checked ) {

        if ( beforeButton != radio[0] ) {

            if ( beginMinute === 0 ) {
                beginMinute = "00";
            }
            else if ( beginMinute > 0 && beginMinute < 10 ) {
                beginMinute = "0" + beginMinute;
            }

            if ( beginSecond === 0 ) {
                beginSecond = "00";
            }
            else if ( beginSecond > 0 && beginSecond < 10 ) {
                beginSecond = "0" + beginSecond;
            }

            if ( endMinute === 0 ) {
                endMinute = "00";
            }
            else if ( endMinute > 0 && endMinute < 10 ) {
                endMinute = "0" + endMinute;
            }

            if ( endSecond === 0 ) {
                endSecond = "00";
            }
            else if ( endSecond > 0 && endSecond < 10 ) {
                endSecond = "0" + endSecond;
            }


            document.getElementById('time_start').value = beginHour + ":" + beginMinute + ":" + beginSecond;
            document.getElementById('time_finish').value = endHour + ":" + endMinute + ":" + endSecond;
        }

        beforeButton = radio[0];

    }
    // ラジオボタンが0~15の時
    else if ( radio[1].checked ) {

        if ( beforeButton != radio[1] ) {

            if ( beginMinuteSecond > 0 * readFileLength ) {

                if ( beginHour < 10 ) {
                    beginHour = "0" + beginHour;
                }

                if ( beginMinute < 10 ) {
                    beginMinute = "0" + beginMinute;
                }

                if ( beginSecond < 10 ) {
                    beginSecond = "0" + beginSecond;
                }

                document.getElementById('time_start').value = beginHour + ":" + beginMinute + ":" + beginSecond;
            }
            else {
                document.getElementById('time_start').value = beginHour + ":" + "00" + ":" + "00";
            }

            if ( endValue < ( 900 * readFileLength - 1 ) ) {

                if ( endHour < 10 ) {
                    endHour = "0" + endHour;
                }
                else if ( endHour === 24 ) {
                    endHour = "00";
                }

                if ( endMinute < 10 ) {
                    endMinute = "0" + endMinute;
                }

                if ( endSecond < 10 ) {
                    endSecond = "0" + endSecond;
                }

                document.getElementById('time_finish').value = endHour + ":" + endMinute + ":" + endSecond;
            }
            else {
                let changeMinute = ( 15 * readFileLength - 1 ) % 60;
                if ( changeMinute === 0 ) {
                    changeMinute = "00";
                }
                document.getElementById('time_finish').value = beginHour + ":" + changeMinute + ":" + "59";
            }
        }

        beforeButton = radio[1];

    }
    // ラジオボタンが15~30の時
    else if ( radio[2].checked ) {

        if ( beforeButton != radio[2] ) {
            if ( beginMinuteSecond > 900 * readFileLength ) {

                if ( beginHour < 10 ) {
                    beginHour = "0" + beginHour;
                }

                if ( beginMinute < 10 ) {
                    beginMinute = "0" + beginMinute;
                }

                if ( beginSecond < 10 ) {
                    beginSecond = "0" + beginSecond;
                }

                document.getElementById('time_start').value = beginHour + ":" + beginMinute + ":" + beginSecond;
            }
            else {
                let changeMinute = ( 15 * readFileLength ) % 60;
                if ( changeMinute === 0 ) {
                    changeMinute = "00";
                }
                document.getElementById('time_start').value = beginHour + ":" + changeMinute + ":" + "00";
            }

            if ( endValue < ( 1800 * readFileLength - 1 ) ) {

                if ( endHour < 10 ) {
                    endHour = "0" + endHour;
                }
                else if ( endHour === 24 ) {
                    endHour = "00";
                }

                if ( endMinute < 10 ) {
                    endMinute = "0" + endMinute;
                }

                if ( endSecond < 10 ) {
                    endSecond = "0" + endSecond;
                }

                document.getElementById('time_finish').value = endHour + ":" + endMinute + ":" + endSecond;
            }
            else {
                let changeMinute = ( 30 * readFileLength - 1 ) % 60;
                if ( changeMinute === 0 ) {
                    changeMinute = "00";
                }
                document.getElementById('time_finish').value = beginHour + ":" + changeMinute + ":" + "59";
            }
        }

        beforeButton = radio[2];

    }
    // ラジオボタンが30~45の時
    else if ( radio[3].checked ) {

        if ( beforeButton != radio[3] ) {
            if ( beginMinuteSecond > 1800 * readFileLength ) {

                if ( beginHour < 10 ) {
                    beginHour = "0" + beginHour;
                }

                if ( beginMinute < 10 ) {
                    beginMinute = "0" + beginMinute;
                }

                if ( beginSecond < 10 ) {
                    beginSecond = "0" + beginSecond;
                }

                document.getElementById('time_start').value = beginHour + ":" + beginMinute + ":" + beginSecond;
            }
            else {
                if ( endHour === 24 ) {
                    endHour = "00";
                }
                let changeMinute = ( 30 * readFileLength ) % 60;
                if ( changeMinute === 0 ) {
                    changeMinute = "00";
                }
                document.getElementById('time_start').value = endHour + ":" + changeMinute + ":" + "00";
            }

            if ( endValue < ( 2700 * readFileLength - 1 ) ) {

                if ( endHour < 10 ) {
                    endHour = "0" + endHour;
                }
                else if ( endHour === 24 ) {
                    endHour = "00";
                }

                if ( endMinute < 10 ) {
                    endMinute = "0" + endMinute;
                }

                if ( endSecond < 10 ) {
                    endSecond = "0" + endSecond;
                }

                document.getElementById('time_finish').value = endHour + ":" + endMinute + ":" + endSecond;
            }
            else {
                if ( endHour === 24 ) {
                    endHour = "00";
                }
                let changeMinute = ( 45 * readFileLength - 1 ) % 60;
                if ( changeMinute === 0 ) {
                    changeMinute = "00";
                }
                document.getElementById('time_finish').value = endHour + ":" + changeMinute + ":" + "59";
            }
        }

        beforeButton = radio[3];

    }
    // ラジオボタンが45~60の時
    else if ( radio[4].checked ) {

        if ( beforeButton != radio[4] ) {
            if ( beginMinuteSecond > 2700 * readFileLength ) {

                if ( beginHour < 10 ) {
                    beginHour = "0" + beginHour;
                }

                if ( beginMinute < 10 ) {
                    beginMinute = "0" + beginMinute;
                }

                if ( beginSecond < 10 ) {
                    beginSecond = "0" + beginSecond;
                }

                document.getElementById('time_start').value = beginHour + ":" + beginMinute + ":" + beginSecond;
            }
            else {
                if ( endHour === 24 ) {
                    endHour = "00";
                }
                let changeMinute = ( 45 * readFileLength ) % 60;
                if ( changeMinute === 0 ) {
                    changeMinute = "00";
                }
                document.getElementById('time_start').value = endHour + ":" + changeMinute + ":" + "00";
            }

            if ( endValue < ( 3600 * readFileLength - 1 ) ) {

                if ( endHour < 10 ) {
                    endHour = "0" + endHour;
                }
                else if ( endHour === 24 ) {
                    endHour = "00";
                }

                if ( endMinute < 10 ) {
                    endMinute = "0" + endMinute;
                }

                if ( endSecond < 10 ) {
                    endSecond = "0" + endSecond;
                }

                document.getElementById('time_finish').value = endHour + ":" + endMinute + ":" + endSecond;
            }
            else {
                if ( endHour === 24 ) {
                    endHour = "00";
                }
                document.getElementById('time_finish').value = endHour + ":" + "59" + ":" + "59";
            }
        }

        beforeButton = radio[4];

    }

    // Time
    const startHMS = document.getElementById('time_start').value.split(':');
    const finishHMS = document.getElementById('time_finish').value.split(':');

    // 時間を秒に直す
    const startHH = Number( startHMS[0] );
    const startMM = Number( startHMS[1] );
    if ( startHMS[2] === undefined ) {
        startHMS[2] = '00';
    }
    const startSS = Number( startHMS[2] );
    startTime = startHH * 3600 + startMM * 60 + startSS;

    let finishHH;
    if ( startHH === 23 && listHour === 2 ) {
        finishHH = Number( finishHMS[0] ) + 24;
    }
    else {
        finishHH = Number( finishHMS[0] );
    }
    const finishMM = Number( finishHMS[1] );
    if ( finishHMS[2] === undefined ) {
        finishHMS[2] = '00';
    }
    const finishSS = Number(finishHMS[2]);
    finishTime = finishHH * 3600 + finishMM * 60 + finishSS;

    // ダウンロードボタンの上の時間変更
    document.getElementById("downloadText_bottom").innerHTML = startHMS[0] + ':' + startHMS[1] + ':' + startHMS[2] + ' ~ ' + finishHMS[0] + ':' + finishHMS[1] + ':' + finishHMS[2];

    x_timeRatio = ( finishTime - startTime ) / 2;
    y_timeRatio = ( finishTime - startTime ) / 2;
    z_timeRatio = ( finishTime - startTime ) / 2;

    for ( let i = 0; i < timeData.length; i++ ) {
        if ( startTime === timeData[i]) {
            dataStart = i;
        }
        if ( finishTime === timeData[i] ){
            dataFinish = i;
            break;
        }
    }


    for ( dataStart; dataStart <= dataFinish; dataStart++ ) {
        x_graphData.push( ( timeData[dataStart] - startTime ) / x_timeRatio - 1, ( x_positionData[dataStart] - x_average ) / galHeight, 0 );
        y_graphData.push( ( timeData[dataStart] - startTime ) / y_timeRatio - 1, ( y_positionData[dataStart] - y_average ) / galHeight, 0 );
        z_graphData.push( ( timeData[dataStart] - startTime ) / z_timeRatio - 1, ( z_positionData[dataStart] - z_average ) / galHeight, 0 );
        graphColor.push( 0.0, 1.0, 1.0, 1.0 );
    }





    // 頂点・色情報設定
    x_dataPosition = create_vbo( dx_gl, x_graphData );
    y_dataPosition = create_vbo( dy_gl, y_graphData );
    z_dataPosition = create_vbo( dz_gl, z_graphData );
    x_dataColor = create_vbo( dx_gl, graphColor );
    y_dataColor = create_vbo( dy_gl, graphColor );
    z_dataColor = create_vbo( dz_gl, graphColor );
    x_dataVBOList = [ x_dataPosition, x_dataColor ];
    y_dataVBOList = [ y_dataPosition, y_dataColor ];
    z_dataVBOList = [ z_dataPosition, z_dataColor ];

    //canvas初期化
    dx_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    dx_gl.clear( dx_gl.COLOR_BUFFER_BIT );
    dy_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    dy_gl.clear( dy_gl.COLOR_BUFFER_BIT );
    dz_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    dz_gl.clear( dz_gl.COLOR_BUFFER_BIT );

    // 描画
    dx_gl.useProgram( dx_prg );
    set_attribute( dx_gl, x_boxVBOList, dx_attL, dx_attS );
    dx_gl.drawArrays( dx_gl.LINES, 0, boxLines.length / 3 );
    set_attribute( dx_gl, x_dataVBOList, dx_attL, dx_attS );
    dx_gl.drawArrays( dx_gl.LINE_STRIP, 0, x_graphData.length / 3 );

    dy_gl.useProgram( dy_prg );
    set_attribute( dy_gl, y_boxVBOList, dy_attL, dy_attS );
    dy_gl.drawArrays( dy_gl.LINES, 0, boxLines.length / 3 );
    set_attribute( dy_gl, y_dataVBOList, dy_attL, dy_attS );
    dy_gl.drawArrays( dy_gl.LINE_STRIP, 0, y_graphData.length / 3 );

    dz_gl.useProgram( dz_prg );
    set_attribute( dz_gl, z_boxVBOList, dz_attL, dz_attS );
    dz_gl.drawArrays( dz_gl.LINES, 0, boxLines.length / 3 );
    set_attribute( dz_gl, z_dataVBOList, dz_attL, dz_attS );
    dz_gl.drawArrays( dz_gl.LINE_STRIP, 0, z_graphData.length / 3 );

    // メモリ解放の為、中身を空に
    x_graphData = [];
    y_graphData = [];
    z_graphData = [];

}

// Galが変更された場合
document.getElementById('gal_value').addEventListener( 'change', changeValue, false );

// 時間フォームが変更された場合
document.getElementById('time_start').addEventListener( 'change', changeValue, false );
document.getElementById('time_finish').addEventListener( 'change', changeValue, false );

/*
    CSVデータをダウンロードする処理
*/

function csvDataDownload() {
    // Time
    const startHMS = document.getElementById('time_start').value.split(':');
    const finishHMS = document.getElementById('time_finish').value.split(':');

    // バイナリデータは秒数で記録されているのでグラフで指定した時間を秒に直す
    const startHH = Number( startHMS[0] );
    const startMM = Number( startHMS[1] );
    if ( startHMS[2] === undefined ) {
        startHMS[2] = '00';
    }
    const startSS = Number( startHMS[2] );
    const startTime = startHH * 3600 + startMM * 60 + startSS;

    let finishHH;
    if ( startHH === 23 && listHour === 2 ) {
        finishHH = Number( finishHMS[0] ) + 24;
    }
    else {
        finishHH = Number( finishHMS[0] );
    }
    const finishMM = Number( finishHMS[1] );
    if ( finishHMS[2] === undefined ) {
        finishHMS[2] = '00';
    }
    const finishSS = Number(finishHMS[2]);
    const finishTime = finishHH * 3600 + finishMM * 60 + finishSS;

    let dataStart;
    let dataFinish;
    let csvData = [];
    let csvDate = document.getElementById("acc_date").value + '-' + downloadDay;
    // 1行目に目録を入れる
    csvData.push( [ 'Time', 'XAcc', 'YAcc', 'ZAcc', csvDate, document.getElementById('time_start').value ] );

    // グラフにて指定した時間がバイナリデータの何番目に当たるか探す
    for ( let i = 0; i < timeData.length; i++ ) {
        if ( startTime === timeData[i]) {
            dataStart = i;
        }
        if ( finishTime === timeData[i] ){
            dataFinish = i;
            break;
        }
    }

    const dataStartPosition = timeData[dataStart];

    // データを配列に入れる
    for ( dataStart; dataStart <= dataFinish; dataStart++ ) {
        csvData.push( [ Number( timeData[dataStart] - dataStartPosition ).toFixed(2), Number(x_positionData[dataStart]).toFixed(3), Number(y_positionData[dataStart]).toFixed(3), Number(z_positionData[dataStart]).toFixed(3) ] );
    }

    // 配列をCSV形式にする
    let csvFormat = "";
    for (let i = 0; i < csvData.length; i++) {
        csvFormat += csvData[i] + ",";
        csvFormat += '\r\n';
    }
    csvFormat = csvFormat.slice( 0, -1 );

    // CSVファイルの名前を決める
    const fileName = document.getElementById("acc_place").value + "_" + document.getElementById("acc_date").value + "-" + downloadDay + "_" + startHMS[0] + startHMS[1] + startHMS[2] + '-' + finishHMS[0] + finishHMS[1] + finishHMS[2] + ".csv";

    // 以下CSVファイルの設定
    const bom = new Uint8Array( [ 0xEF, 0xBB, 0xBF ] );
    const blob = new Blob( [ bom, csvFormat ], { "type" : "text/csv" } );

    if ( window.navigator.msSaveBlob ) {
        window.navigator.msSaveBlob( blob, fileName );
        window.navigator.msSaveOrOpenBlob( blob, fileName );
    }
    else {
        document.getElementById("download").href = window.URL.createObjectURL( blob );
        document.getElementById("download").download = fileName;
    }
}