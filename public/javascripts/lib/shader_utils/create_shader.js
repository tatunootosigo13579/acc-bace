function create_shader ( gl, id ) {

    // シェーダを格納する変数
    let shader;
    const scriptElement = document.getElementById( id );
    if ( !scriptElement ) {
        return;
    }

    // scriptタグのtype属性をチェック
    switch ( scriptElement.type ) {

        // 頂点シェーダの場合
        case 'x-shader/x-vertex':
            shader = gl.createShader( gl.VERTEX_SHADER );
            break;

        // フラグメントシェーダの場合
        case 'x-shader/x-fragment':
            shader = gl.createShader( gl.FRAGMENT_SHADER );
            break;

        default:
            return;
    }

    // 生成されたシェーダにソースを割り当てる
    gl.shaderSource( shader, scriptElement.text );

    // シェーダをコンパイルする
    gl.compileShader( shader );

    // シェーダが正しくコンパイルされたかチェック
    if ( gl.getShaderParameter( shader, gl.COMPILE_STATUS ) ) {

        // 成功していたらシェーダを返して終了
        return shader;

    } else {

        // 失敗していたらエラーログをアラートする
        alert( gl.getShaderInfoLog( shader ) );
    }
}

