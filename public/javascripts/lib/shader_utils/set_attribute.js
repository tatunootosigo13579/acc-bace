// VBOをバインドし登録する関数
function set_attribute (gl, vbo, attL, attS) {
    // 引数として受け取った配列を処理する
    for(let i in vbo){
        // バッファをバインドする
        gl.bindBuffer(gl.ARRAY_BUFFER, vbo[i]);

        // attributeLocationを有効にする
        gl.enableVertexAttribArray(attL[i]);

        // attributeLocationを通知し登録する
        gl.vertexAttribPointer(attL[i], attS[i], gl.FLOAT, false, 0, 0);
    }
}