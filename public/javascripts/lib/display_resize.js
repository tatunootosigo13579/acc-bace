/*
    webglのサイズ変更
*/
function display_resize ( gl, id, wideRatio, heightRatio ) {
    const c = document.getElementById( id );
    c.width = window.innerWidth * wideRatio;
    c.height = window.innerHeight * heightRatio;
    gl.viewport( 0.0, 0.0, c.width, c.height );
}