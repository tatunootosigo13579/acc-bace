//クッキーの属性設定
document.cookie = 'SameSite=None; Secure';

const data_graph = document.getElementsByClassName("data_graph");
const x_Text = document.getElementById("x_Text");
const y_Text = document.getElementById("y_Text");
const z_Text = document.getElementById("z_Text");
const x_maxText = document.getElementById("x_maxText");
const y_maxText = document.getElementById("y_maxText");
const z_maxText = document.getElementById("z_maxText");
const dx_top = document.getElementById("dx_top");
const dy_top = document.getElementById("dy_top");
const dz_top = document.getElementById("dz_top");
const x_ave = document.getElementById("x_ave");
const y_ave = document.getElementById("y_ave");
const z_ave = document.getElementById("z_ave");
const dx_average = document.getElementById("dx_average");
const dy_average = document.getElementById("dy_average");
const dz_average = document.getElementById("dz_average");
const x_minText = document.getElementById("x_minText");
const y_minText = document.getElementById("y_minText");
const z_minText = document.getElementById("z_minText");
const dx_bottom = document.getElementById("dx_bottom");
const dy_bottom = document.getElementById("dy_bottom");
const dz_bottom = document.getElementById("dz_bottom");
const time_area = document.getElementsByClassName("time_area");
const time_bottomText = document.getElementById("time_bottom_text");
const time_start = document.getElementById("time_start");
const time_finish = document.getElementById("time_finish");
const gal_value = Number( document.getElementById( "gal_value" ).value );
let selectPlace = document.getElementById("acc_place");
let selectData = document.getElementById("acc_data");

// ダウンロードするCSVファイルの日付
let downloadDay;

//ネットにアクセスできない環境を想定したGoogle Mapに関する処理
let map = '';
try {
    const MyLatLng = new google.maps.LatLng( 35.6811673, 139.7670516 );
    const Options = {
        zoom: 4,                                   //地図の縮尺値
        center: MyLatLng,                          //地図の中心座標
        mapTypeId: google.maps.MapTypeId.ROADMAP   //地図の種類
    };
    map = new google.maps.Map( document.getElementById('map'), Options );
}
catch (e) {
}

// canvasのWebGL設定
display_resize( dx_gl, 'dx_canvas', 0.4, 0.2 );
display_resize( dy_gl, 'dy_canvas', 0.4, 0.2 );
display_resize( dz_gl, 'dz_canvas', 0.4, 0.2 );

// グラフの平均の文字と値を各グラフの中央に配置する
// ave: □
x_ave.style.top = ( dx_c.height / 2 + x_ave.clientHeight / 2 ) + "px";
y_ave.style.top = ( dy_c.height / 2 + y_ave.clientHeight / 2 ) + "px";
z_ave.style.top = ( dz_c.height / 2 + z_ave.clientHeight / 2 ) + "px";
dx_average.style.top = ( dx_c.height / 2 + dx_average.clientHeight / 2 ) + "px";
dy_average.style.top = ( dy_c.height / 2 + dy_average.clientHeight / 2 ) + "px";
dz_average.style.top = ( dz_c.height / 2 + dz_average.clientHeight / 2 ) + "px";

// グラフの最大値の文字と値を各グラフの上部に合わせて配置する
// max: □
x_minText.style.top = dx_c.height + "px";
y_minText.style.top = dy_c.height + "px";
z_minText.style.top = dz_c.height + "px";
dx_min.style.top = dx_c.height + "px";
dy_min.style.top = dy_c.height + "px";
dz_min.style.top = dz_c.height + "px";

// グラフの最小値の文字と値を各グラフの下部に合わせて配置する
// min: □
time_start.style.right = dz_c.width - time_start.clientWidth / 2 + "px";
time_finish.style.right = - time_finish.clientWidth / 2 + "px";
time_bottomText.style.left = ( time_area[0].clientWidth - dz_c.width - time_start.clientWidth ) / 2 + "px";

// XYZのグラフの上下の値
// 初期は1と-1
dx_top.innerHTML = gal_value;
dy_top.innerHTML = gal_value;
dz_top.innerHTML = gal_value;
dx_bottom.innerHTML = - gal_value;
dy_bottom.innerHTML = - gal_value;
dz_bottom.innerHTML = - gal_value;

// XYZのグラフの上下の値の位置
dx_top.style.top = x_maxText.clientHeight - ( dx_top.clientHeight - x_Text.clientHeight ) + "px";
dy_top.style.top = y_maxText.clientHeight - ( dy_top.clientHeight - y_Text.clientHeight ) + "px";
dz_top.style.top = z_maxText.clientHeight - ( dz_top.clientHeight - z_Text.clientHeight ) + "px";
dx_bottom.style.top = dx_top.clientHeight / 2 + dx_c.height + "px";
dy_bottom.style.top = dy_top.clientHeight / 2 + dy_c.height + "px";
dz_bottom.style.top = dz_top.clientHeight / 2 + dz_c.height + "px";

dx_top.style.left = data_graph[0].clientWidth - dx_c.width - 65 + "px";
dy_top.style.left = data_graph[0].clientWidth - dy_c.width - 65 + "px";
dz_top.style.left = data_graph[0].clientWidth - dz_c.width - 65 + "px";
dx_bottom.style.left = data_graph[0].clientWidth - dx_c.width - 65 + "px";
dy_bottom.style.left = data_graph[0].clientWidth - dy_c.width - 65 + "px";
dz_bottom.style.left = data_graph[0].clientWidth - dz_c.width - 65 + "px";



// ウィンドウサイズを変更した時の動作
// やってることは先述したものと同じ
window.onresize = () => {
    display_resize( dx_gl, 'dx_canvas', 0.4, 0.2 );
    display_resize( dy_gl, 'dy_canvas', 0.4, 0.2 );
    display_resize( dz_gl, 'dz_canvas', 0.4, 0.2 );
    x_ave.style.top = ( dx_c.height / 2 + x_ave.clientHeight / 2 ) + "px";
    y_ave.style.top = ( dy_c.height / 2 + y_ave.clientHeight / 2 ) + "px";
    z_ave.style.top = ( dz_c.height / 2 + z_ave.clientHeight / 2 ) + "px";
    dx_average.style.top = ( dx_c.height / 2 + dx_average.clientHeight / 2 ) + "px";
    dy_average.style.top = ( dy_c.height / 2 + dy_average.clientHeight / 2 ) + "px";
    dz_average.style.top = ( dz_c.height / 2 + dz_average.clientHeight / 2 ) + "px";
    x_minText.style.top = dx_c.height + "px";
    y_minText.style.top = dy_c.height + "px";
    z_minText.style.top = dz_c.height + "px";
    dx_min.style.top = dx_c.height + "px";
    dy_min.style.top = dy_c.height + "px";
    dz_min.style.top = dz_c.height + "px";
    time_start.style.right = dz_c.width - time_start.clientWidth / 2 + "px";
    time_finish.style.right = - time_finish.clientWidth / 2 + "px";
    time_bottomText.style.left = ( time_area[0].clientWidth - dz_c.width - time_start.clientWidth ) / 2 + "px";
    dx_top.style.top = x_maxText.clientHeight - ( dx_top.clientHeight - x_Text.clientHeight ) + "px";
    dy_top.style.top = y_maxText.clientHeight - ( dy_top.clientHeight - y_Text.clientHeight ) + "px";
    dz_top.style.top = z_maxText.clientHeight - ( dz_top.clientHeight - z_Text.clientHeight ) + "px";
    dx_bottom.style.top = dx_top.clientHeight / 2 + dx_c.height + "px";
    dy_bottom.style.top = dy_top.clientHeight / 2 + dy_c.height + "px";
    dz_bottom.style.top = dz_top.clientHeight / 2 + dz_c.height + "px";
    dx_top.style.left = data_graph[0].clientWidth - dx_c.width - 65 + "px";
    dy_top.style.left = data_graph[0].clientWidth - dy_c.width - 65 + "px";
    dz_top.style.left = data_graph[0].clientWidth - dz_c.width - 65 + "px";
    dx_bottom.style.left = data_graph[0].clientWidth - dx_c.width - 65 + "px";
    dy_bottom.style.left = data_graph[0].clientWidth - dy_c.width - 65 + "px";
    dz_bottom.style.left = data_graph[0].clientWidth - dz_c.width - 65 + "px";
};

// キャンバス初期化
dx_gl.clearColor( 0.0, 0.0, 0.0, 1.0  );
dx_gl.clear( dx_gl.COLOR_BUFFER_BIT );
dy_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
dy_gl.clear( dy_gl.COLOR_BUFFER_BIT );
dz_gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
dz_gl.clear( dz_gl.COLOR_BUFFER_BIT );

// エクセルファイルの中身取得
// fetch APIを使用
const getPlace_method = "GET";
const getPlace_mode = "same-origin";
const getPlace_credentials = "include";

// グーグルマップのマーカー関連
let markerLocation, marker = [], infoWindow = [];
let locationDuplicate = 0;

// Locationの値をサーバーのacc_pos_database.xlsxから取ってくる
fetch( "./getPlace", { getPlace_method, getPlace_mode, getPlace_credentials } )
.then( res => res.json() )
.then( place => {
    // DOMに一つ一つ反映させると処理が重くなるので追加するものをフラグメントにまとめてから反映する
    let fragment = document.createDocumentFragment();
    for ( let i = 0; i < place.placeValue.length; i++ ) {
        // 空のエレメントを用意
        let optionPlace = document.createElement("option");
        // value(エクセルファイルB列)とtext(エクセルファイルG列)を使用する
        // valueは後の加速度データなどのファイルにアクセスする際に使用
        // textはhtmlのLocation:に表示させる設置個所に使用
        optionPlace.value = place.placeValue[i];
        optionPlace.text = place.placeText[i];
        // fragmentに追加
        fragment.appendChild(optionPlace);

        // Google mapが使用できる場合はマーカーを設置する
        if ( map != '' ) {
            if ( i >= 1 ) {
                // 1つ前に設定された箇所と住所が違う場合
                if ( place.placeAddress[i] != place.placeAddress[ i - 1 ] ) {
                    // 場所はかぶっていないため特に変更なし
                    locationDuplicate = 0;
                // 1つ前に設定された箇所と住所が同じ場合
                } else {
                    // 場所がかぶっているため表示位置をずらす
                    locationDuplicate++;
                }
            }
            // google mapピン止め
            markerLocation = new google.maps.LatLng( { lat: Number( place.placeLatitude[i] ) + 0.00001 * locationDuplicate, lng: Number( place.placeLongitude[i] ) + 0.00001 * locationDuplicate } );
            marker[i] = new google.maps.Marker ( {
                position: markerLocation,
                map: map
            } );
            // 吹き出しの中身
            infoWindow[i] = new google.maps.InfoWindow ( {
                content: '<div class="sample">' + place.placeText[i] + '<p>' + place.placeDirection[i] + '</p>' +'<img src="public/images/' + place.placeImage[i] + '"width="90" height="90" /></div>',
                place: place.placeText[i]
            } );
            markerEvent(i);
        }
    }
    // DOMに反映
    selectPlace.appendChild(fragment);
} )
.catch( console.error );

// マーカーをクリックした際に吹き出しを表示
function markerEvent(i) {
    marker[i].addListener( 'click', () => {
        infoWindow[i].open( map, marker[i] );
        selectPlace.options[ i + 1 ].selected = true;
    } );
}

// 場所と日付を選択した際のPOSTリクエスト
function listPostRequest() {
    // 月日時毎のXYZそれぞれの最大値と最小値の差が表示されたリストの合計数を取得する
    const listNum = selectData.length;

    // 以前の検索結果が残っている場合はリストの中身を空にする
    if ( listNum != 0) {
        while ( selectData.firstChild ) {
            selectData.removeChild(selectData.firstChild);
        }
    }

    const method = "POST";
    const body = JSON.stringify( {
        accPlace: document.getElementById('acc_place').value,
        accDate: document.getElementById('acc_date').value
    } );
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    // DOMに一つ一つ反映させると処理が重くなるので追加するものをフラグメントにまとめてから反映する
    let fragment = document.createDocumentFragment();
    fetch( "./posDate", { method, headers, body } )
    .then( res => res.json() )
    .then( acc => {

        // データ(acc.amplitude)がある場合
        try {
            // データを改行毎に区切る
            const accArray = acc.amplitude.split('\n');
            for ( let i = 1; i < accArray.length; i++ ) {
                // 中身が無い場合は処理を止める
                if ( accArray[i] === "" ) {
                    break;
                }
                // 続いて,毎にデータを区切る
                const data = accArray[i].split(',');
                let optionList = document.createElement("option");
                // valueとtext要素を追加
                // valueはデータの場所、年、月、日、時を_で区切った状態で追加
                optionList.value = acc.place + '_' + acc.year  + '_' +  data[0] + '_' + data[1] + '_' + data[2];
                if ( Number(data[2]) < 10 ) {
                    data[2] = '0' + data[2];
                }
                // textはデータの月、日、時、XYZそれぞれの最大値と最小値の差を追加
                optionList.text = data[0] + '/' + data[1] + ':' + data[2] + '\u00a0\u00a0\u00a0\u00a0\u00a0' + data[3] + ', ' + data[4] + ', ' + data[5];
                optionList.text.style = "white-space: pre;";
                // フラグメントに追加
                fragment.appendChild(optionList);
            }
            // DOMに反映
            selectData.appendChild(fragment);
        }
        // データ(acc.amplitude)がない場合
        catch (e) {
            alert("Data doesn't exist. Please select other months.");
        }
    } )
    .catch( console.error );
}

// Listを選択した際のPOSTリクエスト
function dataPostRequest() {
    const listHour = Number( document.getElementById('listHour').value );
    const method = "POST";
    const body = JSON.stringify( {
        accData: document.getElementById('acc_data').value,
        listHour: listHour
    } );
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    };
    fetch( "./xyzData", { method, headers, body } )
    .then( res => res.json() )
    .then( acc => {
        // 選択した日付を保存しておく
        downloadDay = acc.day;
        // 1時間分のデータか2時間分のデータかで処理を分ける
        switch ( listHour ) {
            // 1時間
            case 1:
                contentLoad( acc.xyzData.data, 0 );
                break;

            // 2時間
            case 2:
                contentLoad( acc.xyzData, acc.xyzDataSecondStart );
                break;

            default:
                break;
        }
    } )
    .catch( console.error );
}

// mapボタン周り
( function($) {
    const $aside = $('.side_bar > aside');
    $aside.find('button')
    .on( 'click', function () {
        // openクラスが指定されていない場合は追加、指定されている場合は削除
        $aside.toggleClass('open');
        if ( $aside.hasClass('open') ) {
            // グーグルマップを開く
            $aside.stop(true).animate( { right: '-18vw' }, 300, 'easeOutBack' );
        } else {
            // グーグルマップを閉じる
            $aside.stop(true).animate( { right: '-90vw' }, 300, 'easeInBack' );
        }
    } );
} ) (jQuery);











