const express = require('express');
const router = express.Router();
const fs = require('fs');
const xlsx = require('xlsx');

// 月を数字から文字に変換する関数
function changeMonth( month ) {
  let renameMonth;
  switch ( month ) {
    case 1:
      renameMonth = 'Jan';
      break;

    case 2:
      renameMonth = 'Feb';
      break;

    case 3:
      renameMonth = 'Mar';
      break;

    case 4:
      renameMonth = 'Apr';
      break;

    case 5:
      renameMonth = 'May';
      break;

    case 6:
      renameMonth = 'Jun';
      break;

    case 7:
      renameMonth = 'Jul';
      break;

    case 8:
      renameMonth = 'Aug';
      break;

    case 9:
      renameMonth = 'Sep';
      break;

    case 10:
      renameMonth = 'Oct';
      break;

    case 11:
      renameMonth = 'Nov';
      break;

    case 12:
      renameMonth = 'Dec';
      break;

    default:
      break;
  }
  return renameMonth;
}

/* GET home page. */
router.get( '/', ( req, res ) => {
  res.render( 'index' );
} );

// エクセルファイルを読み込む関数
router.get( '/getPlace', ( req, res ) => {
  // エクセルファイル読み込み
  const accPositionDatabase = xlsx.readFileSync( __dirname + '/acc_data/acc_pos_database.xlsx' );
  // acc_pos_database.xlsxのSheet1を読み込む
  const accPositionWorksheet = accPositionDatabase.Sheets.Sheet1;
  // エクセルの末端の行数を取得するために!refを読み込む
  const range = accPositionWorksheet['!ref'].split(':');
  // エクセルの末端の行数を取得
  const placeNum = range[1].slice(1);

  // 必要なデータを読み込む
  // B列：FileName
  // C列：Latitude
  // D列：Longitude
  // F列：Direction(x,y,z)
  // G列：ObjName
  // H列：Address
  // L列：Image
  let accPositionValue = [], accPositionText = [], accPositionLatitude = [], accPositionLongitude = [], accPositionDirection = [], accPositionAddress = [], accPositionImage = [];
  for( let i = 2; i <= placeNum; i++ ) {
    accPositionValue[ i - 2 ] = accPositionWorksheet[ 'B' + i ].v;
    accPositionLatitude[ i - 2 ] = accPositionWorksheet[ 'C' + i ].v;
    accPositionLongitude[ i - 2 ] = accPositionWorksheet[ 'D' + i ].v;
    accPositionDirection[ i - 2 ] = accPositionWorksheet[ 'F' + i ].v;
    accPositionText[ i - 2 ] = accPositionWorksheet[ 'G' + i ].v;
    accPositionAddress[ i - 2 ] = accPositionWorksheet[ 'H' + i ].v;
    accPositionImage[ i - 2 ] = accPositionWorksheet[ 'L' + i ].v;
  }
  // フロント側にファイルから取得した情報を送る
  res.send( {
    placeValue: accPositionValue,
    placeLatitude: accPositionLatitude,
    placeLongitude: accPositionLongitude,
    placeDirection: accPositionDirection,
    placeText: accPositionText,
    placeAddress: accPositionAddress,
    placeImage: accPositionImage
  } );
} );

// Listファイルを読み込む関数
router.post( '/posDate', ( req, res ) => {
  // フロント側から場所と日付を受け取る
  const place = req.body.accPlace;
  const date = req.body.accDate;
  // 日付を-毎に区切って年月日に分ける
  // 2020-3-30を[2020, 3, 30]のようにする
  const divideDate = date.split('-');
  const year = divideDate[0];
  let month = Number(divideDate[1]);
  // フォルダの月は英語表記であるためそれに合わせる
  const renameMonth = changeMonth( month );
  if ( month < 10 ) {
    month = "0" + month;
  }
  // リストファイルの指定
  const amplitudeFile = __dirname + '/acc_data/' + place + '/' + year + '/' + renameMonth + '/' + 'List_' + place + '_' + year + '_' + month + '.csv';
  // ファイル読み込み
  try {
    // ファイルが存在する場合は値と場所、年の情報をフロント側に返す
    fs.statSync(amplitudeFile);
    const amplitudeContents = fs.readFileSync( amplitudeFile, 'utf-8' );
    return res.send( { amplitude: amplitudeContents, place: place, year: year } );
  }
  catch (err) {
    // ファイルが存在しない場合は場所と年の情報のみをフロント側に返す
    return res.send( { place: place, year: year } );
  }
} );

// 加速度データファイル読み込み
router.post( '/xyzData', ( req, res ) => {
  // フロント側でリストの中から選択された情報と読み込むファイルの数を受け取る
  const selectFile = req.body.accData.split('_');
  const listHour = Number( req.body.listHour );
  // 月日時は"01"や"02"表記にしたい為、数字が一桁の場合は0を先頭につける
  for (let i = 2; i <= 4; i++ ) {
    if ( selectFile[i] < 10 ) {
      selectFile[i] = '0' + selectFile[i];
    }
  }
  let month = Number(selectFile[2]);
  // フォルダの月は英語表記であるためそれに合わせる
  let renameMonth = changeMonth( month );
  // 加速度データを読み込む
  let xyzFile = __dirname + '/acc_data/' + selectFile[0] + '/' + selectFile[1] + '/' + renameMonth + '/' + selectFile[3] + '/' + selectFile[0] + '_' + selectFile[1] + '_' + selectFile[2] + '_' + selectFile[3] + '_' + selectFile[4] + '.acb';
  const accFirstData = fs.readFileSync( xyzFile, ( err, data ) => {
    if ( err ) {
      next( err );
    }
  } );
  const day = selectFile[3];

  // 読み込みが1時間分の場合はこの時点でデータと日を送り返す
  if ( listHour === 1 ) {
    res.send( { xyzData: accFirstData, day: day } );
  }

  // 2ファイル目を読み込む準備
  selectFile[4] = Number( selectFile[4] ) + 1;
  if ( selectFile[4] < 10 ) {
    selectFile[4] = '0' + selectFile[4];
  }
  if ( selectFile[4] === 24 ) {
    selectFile[4] = "00";
    selectFile[3] = Number( selectFile[3] ) + 1;
    if ( selectFile[3] < 10 ) {
      selectFile[3] = '0' + selectFile[3];
    }
  }
  // 先に読み込んだ加速度データから1時間後のデータを読み込む
  xyzFile = __dirname + '/acc_data/' + selectFile[0] + '/' + selectFile[1] + '/' + renameMonth + '/' + selectFile[3] + '/' + selectFile[0] + '_' + selectFile[1] + '_' + selectFile[2] + '_' + selectFile[3] + '_' + selectFile[4] + '.acb';
  const accSecondData = fs.readFileSync( xyzFile, ( err, data ) => {
    if ( err ) {
      next( err );
    }
  } );
  // 読み込んだデータを１つの配列にまとめる
  let accTotalData = [];
  for ( let i = 0; i < accFirstData.length; i++ ) {
    accTotalData.push( accFirstData[i] );
  }
  for ( let i = 0; i < accSecondData.length; i++ ) {
    accTotalData.push( accSecondData[i] );
  }
  // データと1時間後のデータが始まる配列の箇所、日を送り返す
  // 配列は0から始まるので初めのデータの数を設定すればOK
  res.send( { xyzData: accTotalData, xyzDataSecondStart: accFirstData.length, day: day } );

} );

module.exports = router;
